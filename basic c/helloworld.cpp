/***************************************************************************//**
 * Copyright(c) 2021, Volansys Technologies
 * 
 * Description:
 * @file  helloworld.cpp	
 * @brief (Brief description/summary of the file.)
 *    
 * A description of the entire functional block. This description should include
 * design information and details which describe the design in more detail than 
 * that found in the requirement.
 * 
 * Author       - ayush oza
 *
 *******************************************************************************
 *
 * History
 *       
 * Dec/29/2021, Ayush Oza, Created (description)
 *
 ******************************************************************************/





/**
* @file         helloworld.cpp
* @author       Ayush
* @include      string.h
*/
#include <iostream>
using namespace std;
/**
* @class   hello
* @brief   simple brief intro
* @detail  detailed intro
*/
class hello{
public:
/**
* @class   hello
* @fn      helloworld
* @brief   print helloworld on the terminal
*/
    void helloworld(){
        cout<<"hello world";
    }
/**
* @class   hello
* @fn      add
* @brief   prints addition of two numbers
*/
    void add(int a, int b){
	    cout<<"adding two numbers:"<<a+b<<endl;
    }
};
int main(){
   int x, y;
   cout<<"Hello world";
   cout<<"\n";
   hello obj1;
   obj1.helloworld();
   obj1.add(x, y);
   return 0;
}
